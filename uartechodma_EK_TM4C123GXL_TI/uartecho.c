/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== uartecho.c ========
 */

/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Timestamp.h>
#include <xdc/runtime/Types.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Clock.h>

/* TI-RTOS Header files */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/UART.h>

/* Example/Board Header files */
#include "Board.h"

#include <stdint.h>
#include <string.h>

static UART_Handle uart;

Void readFxn(UArg arg0, UArg arg1)
{
    #define INPUT_SIZE 100
    static char input[INPUT_SIZE];
    static unsigned int inputIdx = 0;

    for(;;)
    {
        /* Read characters but no more than buffer size */
        unsigned int numRead = UART_read(uart, input+inputIdx, INPUT_SIZE-inputIdx-1);
        if (numRead)
        {
            inputIdx += numRead;

            /* Check for complete line */
            Bool found = FALSE;
            char line[INPUT_SIZE];
            for (unsigned int i = 0 ; i < inputIdx && !found ; ++i)
            {
                if (input[i] == '\n' || input[i] == '\r')
                {
                    /* Line found */
                    memcpy(line,input,i);
                    line[i] = NULL;
                    System_printf("Line %s\n", line);
                    ++i;

                    /* Keep any leftovers */
                    if (i < inputIdx)
                    {
                        inputIdx -= i;
                        memmove(input,&input[i],inputIdx);
                    }
                    else
                    {
                        inputIdx = 0;
                    }

                    /* Ensure string is terminated */
                    input[inputIdx] = NULL;
                    found = TRUE;
                }
            }
        }

        Task_sleep(1);
    }
}

Void taskFxn(UArg arg0, UArg arg1)
{
    Types_FreqHz timestampFreq;
    Timestamp_getFreq(&timestampFreq);
    Uint32 timestampFactor = timestampFreq.lo/1000000;
    System_printf("timestampFreq = %d %d %d\n", timestampFreq.hi, timestampFreq.lo, timestampFactor);

    Uint32 count = 0;
    for(;;)
    {
        Uint32 ticksStart = Clock_getTicks();
        Task_sleep(1000 * 0.001 * Clock_tickPeriod);
        Uint32 ticksEnd = Clock_getTicks();
        System_printf("%8d task0Fxn %d %d %d %d %d\n",
                      Timestamp_get32() / timestampFactor,
                      arg0,
                      count++,
//                      Clock_getTickPeriod(),
//                      Clock_tickPeriod,
                      ticksStart,
                      ticksEnd,
                      ticksEnd - ticksStart);

        System_printf("%8d task0Fxn %d %d\n",
                      Timestamp_get32() / timestampFactor,
                      arg0,
                      Clock_getTicks());
    }
}

Void flushFxn(UArg arg0, UArg arg1)
{
    for(;;)
    {
        System_flush();
        Task_sleep(1);
    }
}

/*
 *  ======== main ========
 */
int main(void)
{
    /* Call board init functions */
    Board_initGeneral();
    Board_initGPIO();
    Board_initUART();

    UART_Params uartParams;

    /* Create a UART with data processing off. */
    UART_Params_init(&uartParams);
    uartParams.readTimeout = 100;
    uartParams.writeDataMode = UART_DATA_BINARY;
    uartParams.readDataMode = UART_DATA_BINARY;
    uartParams.readReturnMode = UART_RETURN_FULL;
    uartParams.readEcho = UART_ECHO_OFF;
    uartParams.baudRate = 115200;
    uart = UART_open(Board_UART0, &uartParams);

    if (uart == NULL) {
        System_abort("Error opening the UART");
    }

    /* Turn on user LED */
    GPIO_write(Board_LED0, Board_LED_ON);

    /* Start BIOS */
    BIOS_start();

    /* This example has logging and many other debug capabilities enabled */
    System_printf("This example does not attempt to minimize code or data "
                  "footprint\n");

    System_printf("Starting the UART Echo example\nSystem provider is set to "
                  "SysMin. Halt the target to view any SysMin contents in "
                  "ROV.\n");

    return (0);
}

Void SystemPrint_OutputFunc(const char *ptr, unsigned int len)
{
    if (len)
    {
        UART_write(uart, ptr, len);
    }
}
