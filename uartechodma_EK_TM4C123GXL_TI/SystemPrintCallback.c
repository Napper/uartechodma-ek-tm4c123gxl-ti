
#include <xdc/runtime/Startup.h>
#include <xdc/runtime/Gate.h>

#include <string.h>

extern Void SystemPrint_OutputFunc(const char *ptr, unsigned int len);

Void SystemPrint_Abort(CString str);
Void SystemPrint_Exit(Int stat);
Void SystemPrint_Flush(Void);
Void SystemPrint_Putch(Char ch);
Bool SystemPrint_Ready(Void);

#define BUFFSIZE 200
static char outbuf[BUFFSIZE];
static unsigned int outidx = 0;
static Bool flushAtExit = FALSE;
static Bool overflowed = FALSE;

Void SystemPrint_Abort(CString str)
{
    if (str != NULL)
    {
        Char ch;
        while ((ch = *str++) != '\0')
        {
            SystemPrint_Putch(ch);
        }
    }

    /* Only flush if configured to do so */
    if (flushAtExit)
    {
        SystemPrint_Flush();
    }
}

Void SystemPrint_Exit(Int stat)
{
    if (flushAtExit)
    {
        SystemPrint_Flush();
    }
}

Void SystemPrint_Putch(Char ch)
{
    IArg key = Gate_enterSystem();

    if (outidx < BUFFSIZE)
    {
        outbuf[outidx++] = ch;
    }
    else
    {
        overflowed = TRUE;
    }

    Gate_leaveSystem(key);
}

Bool SystemPrint_Ready(Void)
{
    return TRUE;
}

Void SystemPrint_Flush(Void)
{
    IArg key = Gate_enterSystem();

    SystemPrint_OutputFunc(outbuf, outidx);
    if (overflowed)
    {
        static char overflowString[] = "...\n";
        SystemPrint_OutputFunc(overflowString, strlen(overflowString));
    }

    outidx = 0;
    overflowed = FALSE;

    Gate_leaveSystem(key);
}


